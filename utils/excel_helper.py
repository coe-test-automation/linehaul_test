# utils/excel_helper.py
import pandas as pd

def read_excel_data(file_path):
    """
    Read data from an Excel file and return as a DataFrame.

    Args:
        file_path (str): Absolute path to the Excel file.

    Returns:
        pandas.DataFrame: DataFrame containing the data from the Excel file.
    """
    try:
        # Read data from Excel file
        data = pd.read_excel(file_path)
        return data
    except Exception as e:
        print(f"Error reading Excel file: {e}")
        return None
