import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# Define your log format
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

# Define file handler and set formatter
file_handler = logging.FileHandler('test.log')
file_handler.setFormatter(formatter)

# Add file handler to logger
logger.addHandler(file_handler)