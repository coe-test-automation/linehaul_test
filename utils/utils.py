import os
import pandas as pd
def get_project_root():
    return os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def read_excel_data(file_path):

    return pd.read_excel(file_path)
