# utils/screenshot.py
import os
from datetime import datetime
from selenium import webdriver

def capture_screenshot(driver, prefix='screenshot'):
    # Generate filename with timestamp
    timestamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    filename = f"{prefix}_{timestamp}.png"

    # Create screenshots directory if it doesn't exist
    screenshot_dir = os.path.join(os.getcwd(), 'screenshots')
    if not os.path.exists(screenshot_dir):
        os.makedirs(screenshot_dir)

    # Save screenshot
    screenshot_path = os.path.join(screenshot_dir, filename)
    driver.save_screenshot(screenshot_path)
    return screenshot_path
