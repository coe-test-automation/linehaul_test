@echo off
REM Navigate to your project directory
cd /d %~dp0

REM Run the Behave tests with Allure formatter
behave -f allure_behave.formatter:AllureFormatter -o allure-results

REM Generate Allure report
allure generate --clean

REM Open Allure report
allure open

REM Indicate completion
echo Tests and report generation complete.
pause