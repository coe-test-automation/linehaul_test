@echo off
REM Navigate to your project directory
cd /d %~dp0

REM Open Allure report
allure open

REM Indicate completion
echo Allure report opened.
pause