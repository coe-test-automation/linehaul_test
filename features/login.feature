Feature: Validate Registration & Login Functionality
    @regression
    Scenario Outline: As a New Personal User access application and Register
        Given the user is on the landing page of the application
        When the user clicks on Login button
        Then is navigated to Login Page
        And clicks on Register here option
        And chooses option - I want to place an Order
        And chooses option as - Personal Customer
        Then is navigated to Registration Page
        And Enter Personal details like <name>
        And Enter Email
        And Enter Phone as <Phone>
        And Enter CustomerICNumber as <CustomerICNumber>
        And Select State
        And Select City
        And Enter PostCode as <PostCode>
        And Enter Address as <Address>
        And Enter Bank Details i.e. <BankName>
        And Enter BeneficiaryName as <BeneficiaryName>
        And Enter AccountNumber as <AccountNumber>
        And select the checkbox - Please read and accept the privacy policy
        And click on Register
        Then a Success Message is displayed with OK button
        And on clicking OK, User is navigated to home page

         Examples:
         | name 	| Email | Phone | CustomerICNumber | State | City | PostCode | Address |BankName | BeneficiaryName | AccountNumber |
         | 1        | 2     | 3     | 4 		       | 5     | 6    | 7        |8        | 9       |	10	           | 11            |


    @smoke
    Scenario Outline: User logs in with valid credentials
        Given the user is on the login page
        When the user enters valid credentials from Excel row <row_number>
        And clicks the login button
        Then the user should be logged in successfully

        Examples:
        | row_number |
        | 0          |
