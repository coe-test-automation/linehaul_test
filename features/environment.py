from utils.logger import logger
import configparser
from utils.webdriver import get_driver

def before_all(context):
    # Load configuration
    config = configparser.ConfigParser()
    config.read('features/environment.ini')

    context.config = config['environment']
    context.base_url = context.config['base_url']

    # Initialize WebDriver
    context.driver = get_driver()
    logger.info("WebDriver initialized")

def before_scenario(context, scenario):
    logger.info(f"Starting scenario: {scenario.name}")
    context.driver.get(context.base_url)
    logger.info(f"Navigating to base URL: {context.base_url}")

def after_scenario(context, scenario):
    logger.info(f"Finished scenario: {scenario.name}")
def before_step(context, step):
    logger.info(f"Starting step: {step.name}")

def after_step(context, step):
    logger.info(f"Finished step: {step.name}")
    if step.status == 'failed':
        logger.error(f"Step failed: {step.name}")
def after_all(context):
    # Quit WebDriver
    context.driver.quit()
    logger.info("WebDriver quit")



