from behave import given, when, then
from utils.excel_helper import read_excel_data
from page_objects.login_page import LoginPage
import allure
import configparser
from utils.webdriver import get_driver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from utils.screenshot import capture_screenshot
from utils.logger import logger
from faker import Faker
from behave import fixture, use_fixture
import time
import os

excel_path = "D:/Application/Automation/LineHaul/data/test_data.xlsx"
test_data = read_excel_data(excel_path)
fake = Faker()

# ***********feature new*************
@given(u'the user is on the landing page of the application')
def step_impl(context):
    #context.driver = get_driver()
    context.login_page = LoginPage(context.driver)
    #context.driver.get("https://dev.pallet.linehaul.com.my")
    assert context.login_page.is_trackShipment_btn_displayed(), "User is not in Home Page"
@when(u'the user clicks on Login button')
def step_impl(context):
    # logger.info("User clicks on Login button")
    context.login_page.click_logIn_btn()

@then(u'is navigated to Login Page')
def step_impl(context):
    assert context.login_page.is_loginPage_label_displayed(), "User is not navigated to Login Page"

@then(u'clicks on Register here option')
def step_impl(context):
    context.login_page.click_Registration_link()

@then(u'chooses option - I want to place an Order')
def step_impl(context):
    context.login_page.click_placeOrder_btn()

@then(u'chooses option as - Personal Customer')
def step_impl(context):
    context.login_page.click_personalCustomer_button()

@then(u'is navigated to Registration Page')
def step_impl(context):
    assert context.login_page.is_registrationPage_title_displayed(), "User is not navigated to Registration Page"

@then(u'Enter Personal details like {name}')
def step_impl(context,name):
    Uname = test_data.iloc[int(name), 0]
    context.login_page.enter_name_txt(Uname)

@then(u'Enter Email')
def step_impl(context):
    UEmail = fake.email()
    context.login_page.enter_email_txt(UEmail)

@then(u'Enter Phone as {Phone}')
def step_impl(context,Phone):
    UPhone = test_data.iloc[int(Phone), 0]
    context.login_page.enter_phone_txt(UPhone)


@then(u'Enter CustomerICNumber as {CustomerICNumber}')
def step_impl(context,CustomerICNumber):
    UCustomerICNumber = test_data.iloc[int(CustomerICNumber), 0]
    context.login_page.enter_customerICNumber_txt(UCustomerICNumber)


@then(u'Select State')
def step_impl(context):
    # UState = test_data.iloc[int(State), 0]
    context.login_page.select_state_dd()


@then(u'Select City')
def step_impl(context):
    # UCity = test_data.iloc[int(City), 0]
    context.login_page.select_city_dd()


@then(u'Enter PostCode as {PostCode}')
def step_impl(context,PostCode):
    UPostCode = test_data.iloc[int(PostCode), 0]
    context.login_page.enter_postCode_txt(UPostCode)

@then(u'Enter Address as {Address}')
def step_impl(context,Address):
    UAddress = test_data.iloc[int(Address), 0]
    context.login_page.enter_address_txt(UAddress)


@then(u'Enter Bank Details i.e. {BankName}')
def step_impl(context,BankName):
    UBankName = test_data.iloc[int(BankName), 0]
    context.login_page.enter_bankName_txt(UBankName)


@then(u'Enter BeneficiaryName as {BeneficiaryName}')
def step_impl(context,BeneficiaryName):
    UBeneficiaryName = test_data.iloc[int(BeneficiaryName), 0]
    context.login_page.enter_beneficiary_txt(UBeneficiaryName)


@then(u'Enter AccountNumber as {AccountNumber}')
def step_impl(context,AccountNumber):
    UAccountNumber = test_data.iloc[int(AccountNumber), 0]
    context.login_page.enter_accountNo_txt(UAccountNumber)


@then(u'select the checkbox - Please read and accept the privacy policy')
def step_impl(context):
    context.login_page.select_acceptPolicy_chkbox()


@then(u'click on Register')
def step_impl(context):
    context.login_page.click_register_btn()

@then(u'a Success Message is displayed with OK button')
@allure.step("Success Message is displayed with OK button")
@allure.severity(allure.severity_level.CRITICAL)
@allure.feature("Registration Feature")
@allure.story("login Functionality")
def step_impl(context):
    expected_message = "Thank you for registering with us! Please check your email for a verification link to activate your account."
    actual_message = context.login_page.get_success_message()
    assert actual_message == expected_message, f"Expected message '{expected_message}', but got '{actual_message}'"
    screenshot_path = capture_screenshot(context.driver, 'Registration_success.png')
    print(f"Screenshot saved: {screenshot_path}")
    allure.attach(context.driver.get_screenshot_as_png(), name="Reg_screenshot", attachment_type=allure.attachment_type.PNG)

@then(u'on clicking OK, User is navigated to home page')
def step_impl(context):
    context.login_page.click_registerSuccessMsgOK_btn()
def after_scenario(context, scenario):
    context.driver.quit()

#### 2nd

@given('the user is on the login page')
def step_user_on_login_page(context):
    context.login_page = LoginPage(context.driver)
    context.login_page.is_loginPage_label_displayed()
    context.login_page.click_logIn_btn()


@when('the user enters valid credentials from Excel row {row_number}')
def step_user_enters_credentials(context, row_number):
    username = test_data.iloc[int(row_number), 0]
    password = test_data.iloc[int(row_number), 1]

    context.login_page.enter_username(username)
    context.login_page.enter_password(password)

@when('clicks the login button')
def step_user_clicks_login_button(context):
    context.login_page.click_signIn_btn()

@then('the user should be logged in successfully')
def step_user_logged_in_successfully(context):
    try:
        userName_displayed = WebDriverWait(context.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[contains(text(),"ADMIN USER")]'))
        )
        assert userName_displayed.is_displayed(), "login failed"
    except TimeoutException:
        assert False, "login may have failed or the page took too long to load"
    screenshot_path = capture_screenshot(context.driver, 'login_success.png')
    print(f"Screenshot saved: {screenshot_path}")
    allure.attach(context.driver.get_screenshot_as_png(), name="Login_screenshot",
                  attachment_type=allure.attachment_type.PNG)

