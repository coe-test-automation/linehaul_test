# login_page.py
import time
import pyautogui
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import logging


class LoginPage:
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 20)

        # Login Page
        self.loginPage_label = (By.XPATH, '//div[@class="Form-title mb-1" and contains(text(),"Sign In")]')
        self.username_input = (By.XPATH, '//input[@aria-label="Email"]')
        self.password_input = (By.XPATH, '//input[@name="password"]')
        self.signIn_btn = (By.XPATH, '//button[contains(text(),"Sign In")]')

        # Home Page
        self.trackShipment_btn = (By.XPATH, '//button[@id="button-addon2"]')
        self.logIn_btn = (By.XPATH, '//button[contains(text(),"Login")]')

        # Registration Page
        self.registrationPage_title = (By.XPATH, '//*[contains(text(),"Register Here!")]')
        self.newUserRegister_lnk = (By.XPATH, '//div[@class="text-center my-2 signin-link"]/a')
        self.placeOrder_btn = (By.XPATH, '//*[contains(text(),"I want to place an order")]')
        self.personalCustomer_button = (By.XPATH, '//*[contains(text(),"Personal Customer")]')
        self.name_txt = (By.XPATH, '//*[@name="name"]')
        self.email_txt = (By.XPATH, '//*[@name="email"]')
        self.phone_txt = (By.XPATH, '//*[@name="phone"]')
        self.customerICNumber_txt = (By.XPATH, '//*[@name="ic"]')
        self.state_dd = (By.XPATH, '(//div[@class=" css-1xc3v61-indicatorContainer"])[1]')
        self.city_dd = (By.XPATH, '(//div[@class=" css-1xc3v61-indicatorContainer"])[3]')
        self.postCode_txt = (By.XPATH, '//*[@name="postcode"]')
        self.address_txt = (By.XPATH, '//*[@name="address"]')
        self.bankName_txt = (By.XPATH, '//*[@name="bankName"]')
        self.beneficiary_txt = (By.XPATH, '//*[@name="beneficiaryName"]')
        self.accountNo_txt = (By.XPATH, '//*[@name="bankAccount"]')
        self.acceptPolicy_chkbox = (By.XPATH, '//*[@id="flexCheckDefault"]')
        self.register_btn = (By.XPATH, '//button[contains(text(),"Register")]')
        self.registerSuccess_msg = (By.XPATH, '//div[@class="modal-content"]//p')
        self.registerSuccessMsgOK_btn = (By.XPATH, '//div[@class="modal-content"]//button')

    def is_trackShipment_btn_displayed(self):
        try:
            self.wait.until(
                EC.presence_of_element_located(self.trackShipment_btn)
            )
            return True
        except:
            return False

    def is_loginPage_label_displayed(self):
        try:
            self.wait.until(
                EC.presence_of_element_located(self.loginPage_label)
            )
            return True
        except:
            return False

    def is_registrationPage_title_displayed(self):
        try:
            self.wait.until(
                EC.presence_of_element_located(self.registrationPage_title)
            )
            return True
        except:
            return False

    def enter_username(self, username):
        self.driver.find_element(*self.username_input).send_keys(username)

    def enter_password(self, password):
        self.driver.find_element(*self.password_input).send_keys(password)

    def click_signIn_btn(self):
        self.driver.find_element(*self.signIn_btn).click()

    def click_logIn_btn(self):
        self.driver.find_element(*self.logIn_btn).click()

    def click_Registration_link(self):
        self.driver.find_element(*self.newUserRegister_lnk).click()

    def click_placeOrder_btn(self):
        ele_placeOrder_btn = self.wait.until(EC.element_to_be_clickable(self.placeOrder_btn))
        ele_placeOrder_btn.click()

    def click_personalCustomer_button(self):
        self.driver.find_element(*self.personalCustomer_button).click()

    def enter_name_txt(self,name):
        self.driver.find_element(*self.name_txt).send_keys(name)

    def enter_email_txt(self,email):
        self.driver.find_element(*self.email_txt).send_keys(email)

    def enter_phone_txt(self,phone):
        self.driver.find_element(*self.phone_txt).send_keys(phone)

    def enter_customerICNumber_txt(self,icnumber):
        self.driver.find_element(*self.customerICNumber_txt).send_keys(icnumber)

    def select_state_dd(self):
           state_dropdown= self.wait.until(EC.element_to_be_clickable(self.state_dd))
           actions = ActionChains(self.driver)
           actions.click(state_dropdown) \
               .send_keys(Keys.ARROW_DOWN) \
               .send_keys(Keys.ENTER) \
               .send_keys(Keys.TAB) \
               .send_keys(Keys.TAB) \
               .send_keys(Keys.TAB) \
               .send_keys(Keys.TAB) \
               .perform()
    def select_city_dd(self):
           actions = ActionChains(self.driver)
           actions.send_keys(Keys.ARROW_DOWN) \
               .send_keys(Keys.ENTER) \
               .perform()
    def enter_postCode_txt(self,postCode):
        self.driver.find_element(*self.postCode_txt).send_keys(postCode)

    def enter_address_txt(self,address):
        self.driver.find_element(*self.address_txt).send_keys(address)

    def enter_bankName_txt(self,bankName):
        self.driver.find_element(*self.bankName_txt).send_keys(bankName)

    def enter_beneficiary_txt(self,beneficiary):
        self.driver.find_element(*self.beneficiary_txt).send_keys(beneficiary)

    def enter_accountNo_txt(self,account):
        self.driver.find_element(*self.accountNo_txt).send_keys(account)

    def select_acceptPolicy_chkbox(self):
        self.driver.find_element(*self.acceptPolicy_chkbox).click()

    def click_register_btn(self):
        self.driver.find_element(*self.register_btn).click()
        time.sleep(3)

    def get_success_message(self):
        try:
            success_message_element = self.wait.until(EC.presence_of_element_located(self.registerSuccess_msg))
            print("test******************123"+success_message_element.text)
            return success_message_element.text
        except:
            return None

    def click_registerSuccessMsgOK_btn(self):
        self.driver.find_element(*self.registerSuccessMsgOK_btn)
