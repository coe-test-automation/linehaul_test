# LineHaul Test Framework

## Overview
This is a BDD test framework using Python with Behave and Allure for reporting. It allows you to write and run tests in a structured, readable format and generate comprehensive reports.

## Features
- BDD with Behave
- Page Object Model
- Excel based test data management
- Tag-based test filtering
- Allure report generation

## Installation

### Dependencies
- Python 3.x and plus
- pip
- behave
- allure
- selenium

### Installation Instructions
1. Clone the repository:
   ```sh
   git clone https://github.com/
   cd your-repo
